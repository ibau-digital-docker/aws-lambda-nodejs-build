FROM ubuntu:18.04

RUN apt update && apt -y upgrade

RUN apt update \
    && apt install -y --no-install-recommends curl dirmngr apt-transport-https lsb-release ca-certificates zip \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt install -y --no-install-recommends nodejs gcc g++ make

RUN apt-get install -y --no-install-recommends \
    wget python3 python3-dev python3-pip python3-setuptools fakeroot

RUN pip3 install awscli boto3
